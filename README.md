# Digital image processing

APP5 study project
image processing using opencv2

### Part 2:
placer les fragments dans la fresque
* en utilisant les points d’intérêt (FeatureDetector)
* FeaturDescriptor (invariant à la description) pour stocker points d intérêt et les 8 voisins
* -> trouver la bonne paire de détecteur+descripteur de points d’intérêt
* RANSAC pour éliminer les observations aberrantes
* on choisi successivement 2 points, on calcule la rotation et la translation, et on regarde combien d autres points sont bons avec cette combinaison, le résultat le plus grand détermine la bonne solution. (on a donc besoin d'au moins 3 bons points par fragment)
* pas de changement d'échelle -> on peut calculer les distances entre les points (condition nécessaire mais pas suffisante)
