// A2DD.h
#ifndef UTILS_H
#define UTILS_H
//#warning "COMPILE UTILS"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#define ERROR_OPEN(file) "ERROR: cannot open file ["+file+"]"
#define ERROR_PARSE(file) "ERROR: cannot parse file ["+file+"]"

#define ERROR_USAGE(cmd) "ERROR: Invalid use ["<<cmd<<"]\n       Use --help\n"

#define COLOR_GREEN cv::Scalar(0,255,0,255)
#define COLOR_BLACK cv::Scalar(0,0,0,255)
#define COLOR_WHITE cv::Scalar(255,255,255,255)

#define STEP_SIZE 20

#define IMG_WIDTH  1707
#define IMG_LENGHT 775

#define NB_FRAGMENTS        327

#define TEST_IMG "./res/image.png"

void dispInfo()
{
  std::cout << "\nDigital image processing Project" << std::endl;
  std::cout << "Dorian Veriere & Thibault Delorme" << std::endl;
  std::cout << "      APP5 Info, 2019/2020" << std::endl;
  std::cout << "Currently running with OpenCV version : " << CV_VERSION << std::endl;
  std::cout << "Developped and tested with :\n\t- OpenCV version :\n\t- 2.4.13\n\t- 3.4.2\n\t- 4.1.0" << std::endl;
}

cv::Mat rot(const cv::Mat img, const int deg)
{
  cv::Mat dst;
  //cv::Point2f rot_center(3*img.rows/4, 3*img.cols/4); // center
  cv::Point2f rot_center(img.cols/2, img.rows/2); // top left corner

  cv::Mat rot_mat = cv::getRotationMatrix2D(rot_center, deg, 1.);

  //std::cout << "Rotation Matrix = : " << rot_mat << std::endl;

  // adjust transformation matrix
  //rot_mat.at<double>(0,2) += bbox.width/2.0 - img.cols/2.0;
  //rot_mat.at<double>(1,2) += bbox.height/2.0 - img.rows/2.0;

  warpAffine(img, dst, rot_mat, img.size());
  return dst;
}

uint copy(cv::Mat dest, const cv::Mat copy, uint posx, uint posy)
{
  uint ret = 0;
  if((posx + copy.cols > dest.cols || posy + copy.rows > dest.rows))
  {
    std::cerr<<"ERROR: problem of size in copy"<<std::endl;
    return ret;
  }

  for (int i = 0; i<copy.rows; i++)
  {
    for (int j = 0; j<copy.rows; j++)
    {
      if(copy.at<cv::Vec4b>(i,j)[3]==255) //if not a transparent pixel
      {
        ret ++;
        dest.at<cv::Vec4b>(i+posx, j+posy) = copy.at<cv::Vec4b>(i,j);
      }
    }
  }
  return ret;
}

#include "fragment.hpp"

uint copy(cv::Mat dest, Fragment piece)
{
  uint ret = 0;
  cv::Mat tmpMat = rot(piece.matrix, piece.getRotation());
  //cv::Mat tmpMat = rot(piece.matrix, piece.getRotation());
  //std::cout<<"row "<<tmpMat.rows<<" cols "<<tmpMat.cols<<std::endl;
  for (int i = 0; i<tmpMat.cols; i++)
  {
    for (int j = 0; j<tmpMat.rows; j++)
    {
      int x = i+piece.getPosition().x - (tmpMat.cols/2);
      int y = j+piece.getPosition().y - (tmpMat.rows/2);
      if(i>=0 && j>=0
        && x>=0 && y>=0
        && x < IMG_LENGHT
        && y < IMG_WIDTH)
        {
          if(tmpMat.at<cv::Vec4b>(i,j)[3]==255) //if not a transparent pixel
          {
            ret++;
            dest.at<cv::Vec4b>(x, y) = tmpMat.at<cv::Vec4b>(i,j);
          }
          // dbg ThD : draw the bordure in aqua
          /*if(i == 0 || j == 0 || i == tmpMat.cols-1 || j == tmpMat.cols-1)
          {
          dest.at<cv::Vec4b>(x, y) = cv::Vec4b(0,0,255,255);
        }*/
      }
    }
  }
  return ret;
}



void demoRot()
{
  int rot = 0;
  Fragment image(TEST_IMG);

  std::cout<<"demonstrate the rotation management (press a key in the opencv window to rotate the piece)"<<std::endl;

  while(true)
  {
    std::cout<<rot<<std::endl;
    image.setRotation(rot);
    image.dispImage();
    cv::waitKey(0);
    rot += 10;
  }
}

#endif
