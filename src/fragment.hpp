#ifndef FRAGMENT_H
#define FRAGMENT_H
//#warning "COMPILE FRAGMENT"

#include "utils.hpp"

#define PATH_FRAGMENTS      "./res/frag_eroded/frag_eroded_"
#define EXTENSION_FRAGMENTS ".png"
#define COLOR_TRANS         cv::Scalar(0,0,0,0)


class Fragment
{
private:
  std::string filePath;
  uint size;
  float rotation;
  cv::Point position;

public:
  cv::Mat matrix;
  Fragment(int idFragment, int x=0, int y=0, float rot=0){
    this->filePath = PATH_FRAGMENTS+std::to_string(idFragment)+EXTENSION_FRAGMENTS;
    //std::cout<<"create fragment nb" << idFragment << " -> " <<filePath<<std::endl;
    // read the file, use IMREAD_UNCHANGED to keep the alpha channel
    cv::Mat tmpMat = cv::imread(filePath, cv::IMREAD_UNCHANGED );
    if(! tmpMat.data )                              // Check for invalid input
    {
      std::cerr<<"Could not open or find the image"<<filePath<<std::endl;
    }
    //use a bigger size to prevent crop after rotation
    uint new_size = tmpMat.cols;
    //std::cout<<"NEW SIZE = "<<new_size<<std::endl;
    matrix = cv::Mat(new_size, new_size, CV_8UC4, COLOR_TRANS); //CV_8UC4 = RGB + gamma
    size = copy(matrix, tmpMat, 0,0);
    position = cv::Point(x,y);
    rotation = rot;
  }

  Fragment(std::string path, int x=0, int y=0, float rot=0){
    this->filePath = path;
    // read the file, use IMREAD_UNCHANGED to keep the alpha channel
    cv::Mat tmpMat = cv::imread(filePath, cv::IMREAD_UNCHANGED );
    if(! tmpMat.data )                              // Check for invalid input
    {
      std::cerr<<"Could not open or find the image"<<filePath<<std::endl;
    }
    //use a bigger size to prevent crop after rotation
    uint new_size = tmpMat.cols;
    //std::cout<<"NEW SIZE = "<<new_size<<std::endl;
    matrix = cv::Mat(new_size, new_size, CV_8UC4, COLOR_TRANS); //CV_8UC4 = RGB + gamma
    size = copy(matrix, tmpMat, 0,0);
    position = cv::Point(x,y);
    rotation = rot;
  }

  void setRotation(int deg)
  {
    rotation = deg;
  }

  int getRotation() const
  {
    return rotation;
  }

  uint getSize() const
  {
    return size;
  }

  void setPosition(cv::Point p){
    position = p;
  }

  cv::Point getPosition() const{
    return cv::Point(position.y, position.x);
  }

  void dispImage()
  {
    cv::Mat tmpMat = rot(matrix, rotation);
    cv::imshow("dispImage", tmpMat);
    cv::resizeWindow("dispImage", tmpMat.cols, tmpMat.cols);
  }
};

#endif
