#ifndef IMAGE_CONSTRUCTOR_H
#define IMAGE_CONSTRUCTOR_H
//#warning "COMPILE IMAGE_CONSTRUCTOR"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>

#include "utils.hpp"
#include "fragment.hpp"

#define HELP_MSG "TPnoteP1_construct [file_to_construct_image_with]"
#define BACK_IMG "./res/theCreationofAdam.jpg"
#define DEBUG_LINES 0


class ImageConstructor
{
private:
  cv::Size size;
  std::vector<Fragment> pieces;
  cv::Mat matrix;

  //draw the back of the image constructor (a grid)
  void clearMat()
  {
    bool res = false;
    if (! DEBUG_LINES)
    {
      cv::Mat image = cv::imread(BACK_IMG, cv::IMREAD_COLOR );
      if(image.data)
      {
        cvtColor(image,image, cv::COLOR_BGR2BGRA);
        for( int y = 0; y < image.rows; y++ )
        { for( int x = 0; x < image.cols; x++ )
          { for( int c = 0; c < 3; c++ )
            {
              image.at<cv::Vec4b>(y,x)[c] = 0.6*( image.at<cv::Vec4b>(y,x)[c] );
            }
          }
        }
        image.copyTo(matrix);
        res = true;
      }
    }
    if (DEBUG_LINES || ! res)
    {
      for (int i = 0; i<matrix.size().height; i += STEP_SIZE)
      {
        cv::line(matrix, cv::Point(0, i), cv::Point(matrix.size().width, i), COLOR_BLACK);
      }
      for (int i = 0; i<matrix.size().width; i += STEP_SIZE)
      {
        cv::line(matrix, cv::Point(i, 0), cv::Point(i, matrix.size().height), COLOR_WHITE);
      }
    }
  }

  //draw all the pieces in the matrix
  void updateMatrix()
  {
    clearMat();
    for(auto piece : pieces)
    {
      //std::cout<<"ADD a piece START"<<std::endl;
      copy(matrix, piece);
      //std::cout<<"ADD a piece FINISHED"<<std::endl;
    }
  }

public:
  ImageConstructor(const uint sizeL, const uint sizeW){
    matrix = cv::Mat(sizeL, sizeW, CV_8UC4, COLOR_GREEN);
    clearMat();
  }

  ImageConstructor(const uint sizeL, const uint sizeW, const std::string file){
    matrix = cv::Mat(sizeL, sizeW, CV_8UC4, COLOR_GREEN);
    clearMat();
    std::ifstream fd(file);
    if(!fd.good())
    {
      throw(ERROR_OPEN(file));
    }
    std::string line;
    int id, x, y;
    float rot;
    while (std::getline(fd, line))
    {
      //std::cout<<line<<std::endl;
      if(sscanf(line.c_str(), "%d %d %d %f ", &id, &x, &y, &rot) != 4){
        throw(ERROR_PARSE(file));
      }
      //std::cout<<id<<" "<<x<<" "<<y<<" "<<rot<<" "<<std::endl;
      add(id, x, y, rot);
    }
  }

  void add(Fragment p, const uint posx=-1, const uint posy=-1, const float deg=-1)
  {
    if(posx != -1 && posy != -1 ){
      p.setPosition(cv::Point(posx,posy));
    }
    if(deg != -1 ){
      p.setRotation(deg);
    }
    pieces.push_back(p);
  }


  cv::Mat getImage()
  {
    updateMatrix();
    return matrix;
  }

  void dispImage()
  {
    updateMatrix();
    imshow("Display window", matrix);
  }
};

#endif
