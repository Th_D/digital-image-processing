#include <iostream>

#include "utils.hpp"
#include "fragment.hpp"
#include "image_constructor.hpp"



int main( int argc, char** argv )
{
  dispInfo();

  //demonstrate the rotation management (press a key in the opencv window to rotate the piece)
  //demoRot();
  if (argc == 2 && (strcmp(argv[1], "--help")==0 || strcmp(argv[1], "--h")==0))
  {
    std::cout<<HELP_MSG<<std::endl;
    return -1;
  }
  else if (argc == 2)
  {
    try
    {
      ImageConstructor *ic = new ImageConstructor(IMG_LENGHT, IMG_WIDTH, argv[1]);
      ic->dispImage();
      cv::waitKey(0);                  // Wait for a keystroke in the window
    }
    catch(std::string const& e)
    {
      std::cerr << e << '\n';
      return -2;
    }
  }
  else
  {
    std::cerr << ERROR_USAGE(argv[0]);
    return -3;
  }
  //Fragment piece23(23);
  //piece23.dispImage();

  //ic->add(piece23, 50, 50,0);
  //ic->add(piece12);

  return 0;
}
