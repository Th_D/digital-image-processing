
#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include<iterator>

#include "utils.hpp"
#include "fragment.hpp"


#define HELP_MSG  "TPnoteP1_eval [correct_answer_file] [file_to_compare]"

#define DEF_DELTA_POS_X 5
#define DEF_DELTA_POS_Y 5
#define DEF_DELTA_ROT   5

std::string correct_fileName;
std::string toCheck_fileName;


bool fillMap(std::map<int,Fragment*>* map, std::ifstream *fd)
{
  int id, x, y;
  float rot;
  std::string line;
  while (std::getline(*fd, line))
  {
    if(sscanf(line.c_str(), "%d %d %d %f ", &id, &x, &y, &rot) != 4){
      return false;
    }
    map->insert(std::pair<int,Fragment*>(id, new Fragment(id, x, y, rot)));
  }
  return true;
}

uint getTotalSize(std::map<int,Fragment*>* map)
{
  uint ret = 0;
  for(std::map<int,Fragment*>::iterator iter = map->begin(); iter != map->end(); ++iter){
    ret += iter->second->getSize();
  }
  return ret;
}

uint goodFragments(std::map<int,Fragment*>* correct, std::map<int,Fragment*>* toCheck, const int delta_pos_x=DEF_DELTA_POS_X, const int delta_pos_y=DEF_DELTA_POS_Y, const int delta_rot = DEF_DELTA_ROT)
{
  int sum = 0;
  for(std::map<int,Fragment*>::iterator iter = correct->begin(); iter != correct->end(); ++iter){
    std::map<int,Fragment*>::iterator found = toCheck->find(iter->first);
    if(found != toCheck->end())
    {
      if(std::abs(iter->second->getRotation()-found->second->getRotation())<= delta_rot
      && std::abs(iter->second->getPosition().x-found->second->getPosition().x)<= delta_pos_x
      && std::abs(iter->second->getPosition().y-found->second->getPosition().y)<= delta_pos_y)
      {
        //std::cout<<iter->first<<" +\n";
        //std::cout<<std::abs(iter->second->getRotation() - found->second->getRotation())<<" ROT\n";
        sum += iter->second->getSize();
      }
    }
  }
  return sum;
}

uint badFragments(std::map<int,Fragment*>* correct, std::map<int,Fragment*>* toCheck)
{
  int sum = 0;
  for(std::map<int,Fragment*>::iterator iter = toCheck->begin(); iter != toCheck->end(); ++iter){
    std::map<int,Fragment*>::iterator found = correct->find(iter->first);
    if(found == correct->end())
    {
      //std::cout<<iter->first<<" -\n";
      sum += iter->second->getSize();
    }
  }
  return sum;
}



float evaluate(std::ifstream* correct_file, std::ifstream* toCheck_file, const int delta_pos_x=DEF_DELTA_POS_X, const int delta_pos_y=DEF_DELTA_POS_Y, const int delta_rot = DEF_DELTA_ROT)
{
  std::map<int,Fragment*> *correctFragments = new std::map<int,Fragment*>();
  std::map<int,Fragment*> *toCheckFragments = new std::map<int,Fragment*>();

  if(!fillMap(correctFragments, correct_file))
  {
    throw(ERROR_PARSE(correct_fileName));
    return -5;
  }

  if (!fillMap(toCheckFragments, toCheck_file))
  {
    throw(ERROR_PARSE(toCheck_fileName));
    return -5;
  }

  uint totalSize = getTotalSize(correctFragments);
  int score = 0;
  score += goodFragments(correctFragments, toCheckFragments, delta_pos_x, delta_pos_y, delta_rot);
  score -= badFragments(correctFragments, toCheckFragments);
  return (score*100.)/totalSize;
}

float evaluate(const std::string correct_filePath, const std::string toCheck_filePath, const int delta_pos_x=DEF_DELTA_POS_X, const int delta_pos_y=DEF_DELTA_POS_Y, const int delta_rot = DEF_DELTA_ROT)
{
  std::ifstream *correctFile;
  std::ifstream *toCheckFile;

  correctFile = new std::ifstream(correct_filePath);
  toCheckFile = new std::ifstream(toCheck_filePath);
  if (!correctFile->good())
  {
    throw(ERROR_OPEN(correct_fileName));
  }
  if (!toCheckFile->good())
  {
    throw(ERROR_OPEN(toCheck_fileName));
  }
  return evaluate(correctFile, toCheckFile, delta_pos_x, delta_pos_y);
}

int main( int argc, char** argv )
{
  dispInfo();

  int score = 0;

  if (argc == 2 && (strcmp(argv[1], "--help")==0 || strcmp(argv[1], "--h")==0))
  {
    std::cout<<HELP_MSG<<std::endl;
    return -2;
  }
  else if (argc==3)
  {
    std::cout<<"Start comparison between:\n\t- "<<argv[1]<<" (correct)\n\t- "<<argv[2]<<" (to compare)\n"<<std::endl;
    correct_fileName = argv[1];
    toCheck_fileName = argv[2];
    try
    {
      score = evaluate(correct_fileName, toCheck_fileName);
      std::cout<<"SCORE= "<<score<<"%"<<std::endl;
    } catch(std::string const& e)
    {
      std::cerr << e << '\n';
    }
  }
  else
  {
    std::cerr << ERROR_USAGE(argv[0]);
    return -1;
  }

  return score;
}
